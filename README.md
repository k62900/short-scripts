# Short Scripts

## Introduction
A collection of small, portable scripts that make life easier.

## Scripts

### Bash

#### alivehosts.sh
Runs an nmap and returns a parsed lists of hosts which are up in the subnet, along with their PTR records if they exist.

### Powershell

#### bulk\_dns\_lookup.ps1
Bulk DNS lookup tool. Will prompt for a file - select a text file with one host per line. Output will be in same folder as script, called DNSLookup_Results.txt

## Contact
www.geekynick.co.uk

www.alwaysnetworks.co.uk

[@GeekyNick](https://twitter.com/GeekyNick)